# frozen_string_literal: true

module Fedex
  class Rates
    attr_reader :credentials, :quote_params

    def self.get(credentials, quote_params)
      payload = Rates::Payload.build(credentials, quote_params)
      # Code goes here...
      # 1. Build body (credentials and parcel info)
      # 2. Call the WS
      # 3. Parse the response
    end
  end
end
