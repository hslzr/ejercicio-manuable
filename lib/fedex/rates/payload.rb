# frozen_string_literal: true

require "dry/schema"

module Fedex
  # rubocop:disable Metrics/ClassLength, Style/Documentation
  class Rates
    class Payload
      CREDENTIALS_SCHEMA = Dry::Schema.Params do
        required(:key).filled(:string)
        required(:password).filled(:string)
        required(:account_number).filled(:string)
        required(:meter_number).filled(:string)
      end

      QUOTE_SCHEMA = Dry::Schema.Params do
        required(:address_from).hash do
          required(:street_lines).filled(:string)
          required(:city).filled(:string)
          required(:state_code).filled(:string).value(format?: /\A[a-zA-Z]{2,4}\z/)
          required(:zip).filled(:string).value(format?: /\A\d+\z/)
          required(:country).filled(:string).value(format?: /\A[a-zA-Z]{2,3}\z/)
        end
        required(:address_to).hash do
          required(:street_lines).filled(:string)
          required(:city).filled(:string)
          required(:state_code).filled(:string).value(format?: /\A[a-zA-Z]{2,4}\z/)
          required(:zip).filled(:string).value(format?: /\A\d+\z/)
          required(:country).filled(:string).value(format?: /\A[a-zA-Z]{2,3}\z/)
        end
        required(:parcel).hash do
          required(:length).filled(:float, gteq?: 0)
          required(:width).filled(:float, gteq?: 0)
          required(:height).filled(:float, gteq?: 0)
          required(:distance_unit).filled(:string)
          required(:weight).filled(:float, gteq?: 0)
          required(:mass_unit).filled(:string)
        end
      end

      def initialize(credentials, quote_params)
        @credentials = {
          language_code: "es",
          locale_code: "MX"
        }.merge(credentials)
        @quote_params = quote_params
        validate_schemas
      end

      def self.build(credentials, quote_params)
        new(credentials, quote_params).xml_payload
      end

      def xml_payload
        <<-XML
      <RateRequest xmlns="http://fedex.com/ws/rate/v13">
      #{authentication_headers}
      <Version>
        <ServiceId>crs</ServiceId>
        <Major>13</Major>
        <Intermediate>0</Intermediate>
        <Minor>0</Minor>
      </Version>
      <ReturnTransitAndCommit>true</ReturnTransitAndCommit>
      #{requested_shipment_details}
      </RateRequest>
        XML
      end

      private

      def authentication_headers
        <<-XML
        <WebAuthenticationDetail>
          <UserCredential>
            <Key>#{@credentials[:key]}</Key>
            <Password>#{@credentials[:password]}</Password>
          </UserCredential>
        </WebAuthenticationDetail>
        <ClientDetail>
          <AccountNumber>#{@credentials[:account_number]}</AccountNumber>
          <MeterNumber>#{@credentials[:meter_number]}</MeterNumber>
          <Localization>
            <LanguageCode>#{@credentials[:language_code]}</LanguageCode>
            <LocaleCode>#{@credentials[:locale_code]}</LocaleCode>
          </Localization>
        </ClientDetail>
        XML
      end

      def requested_shipment_details
        <<-XML
        <RequestedShipment>
          <DropoffType>REGULAR_PICKUP</DropoffType>
          <PackagingType>YOUR_PACKAGING</PackagingType>
          <Shipper>
            <Address>
              <StreetLines>#{@quote_params.dig(:addres_from, :street_lines)}</StreetLines>
              <City>#{@quote_params.dig(:address_from, :city)}</City>
              <StateOrProvinceCode>#{@quote_params.dig(:addres_from, :state_code)}</StateOrProvinceCode>
              <PostalCode>#{@quote_params.dig(:address_from, :zip)}</PostalCode>
              <CountryCode>#{@quote_params.dig(:address_from, :country)}</CountryCode>
            </Address>
          </Shipper>
          <Recipient>
            <Address>
              <StreetLines>#{@quote_params.dig(:address_to, :street_lines)}</StreetLines>
              <City>#{@quote_params.dig(:address_to, :city)}</City>
              <StateOrProvinceCode>#{@quote_params.dig(:address_to, :state_code)}</StateOrProvinceCode>
              <PostalCode>#{@quote_params.dig(:address_to, :zip)}</PostalCode>
              <CountryCode>#{@quote_params.dig(:address_to, :country)}</CountryCode>
              <Residential>false</Residential>
            </Address>
          </Recipient>
          <ShippingChargesPayment>
            <PaymentType>SENDER</PaymentType>
          </ShippingChargesPayment>
          <RateRequestTypes>ACCOUNT</RateRequestTypes>
          <PackageCount>1</PackageCount>
          <RequestedPackageLineItems>
            <GroupPackageCount>1</GroupPackageCount>
            <Weight>
              <Units>#{@quote_params.dig(:parcel, :mass_unit)}</Units>
              <Value>#{@quote_params.dig(:parcel, :weight)}</Value>
            </Weight>
            <Dimensions>
              <Length>#{@quote_params.dig(:parcel, :length)}</Length>
              <Width>#{@quote_params.dig(:parcel, :width)}</Width>
              <Height>#{@quote_params.dig(:parcel, :height)}</Height>
              <Units>#{@quote_params.dig(:parcel, :distance_unit)}</Units>
            </Dimensions>
          </RequestedPackageLineItems>
        </RequestedShipment>
        XML
      end

      def validate_schemas
        validate_credentials
        validate_quote_params
      end

      def validate_credentials
        creds_schema = CREDENTIALS_SCHEMA.call(@credentials)
        return if creds_schema.success?

        raise CredentialsError, creds_schema.errors(full: true).to_h.values.first&.first
      end

      def validate_quote_params
        quote_schema = QUOTE_SCHEMA.call(@quote_params)
        return if quote_schema.success?

        # TODO: Refactor this in the future
        errors = quote_schema.errors(full: true).to_h.first
        # => [:key, {value: ["value message"]}]
        error_message = "#{errors[0]}.#{errors[1].values.first&.first}"
        # => "key.value is missing"
        raise PayloadError, error_message
      end
    end
  end
  # rubocop:enable Metrics/ClassLength, Style/Documentation
end
