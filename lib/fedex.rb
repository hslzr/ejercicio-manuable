# frozen_string_literal: true

require_relative "fedex/version"
require_relative "fedex/rates"
require_relative "fedex/rates/payload"

module Fedex
  class Error < StandardError; end
  class CredentialsError < Error; end
  class PayloadError < Error; end
  # Your code goes here...
end
