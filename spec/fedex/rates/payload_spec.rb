# frozen_string_literal: true

RSpec.describe Fedex::Rates::Payload do
  it "defines a credentials schema" do
    expect(Fedex::Rates::Payload::CREDENTIALS_SCHEMA).not_to be nil
  end

  it "defines a quote schema" do
    expect(Fedex::Rates::Payload::QUOTE_SCHEMA).not_to be nil
  end

  context "credentials" do
    before do
      @quote = { not_relevant_now: "yay" }
    end

    it "validates presence of key" do
      expect { Fedex::Rates::Payload.build({ password: "123" }, @quote) }
        .to raise_error(Fedex::CredentialsError, "key is missing")
    end

    it "validates presence of password" do
      expect { Fedex::Rates::Payload.build({ key: "123" }, @quote) }
        .to raise_error(Fedex::CredentialsError, "password is missing")
    end

    it "validates presence of account_number" do
      expect { Fedex::Rates::Payload.build({ key: "123", password: "123" }, @quote) }
        .to raise_error(Fedex::CredentialsError, "account_number is missing")
    end

    it "validates presence of meter_number" do
      expect { Fedex::Rates::Payload.build({ key: "123", password: "123", account_number: "8976" }, @quote) }
        .to raise_error(Fedex::CredentialsError, "meter_number is missing")
    end
  end

  context "payload" do
    context "raises a Fedex::PayloadError when the payload is not valid" do
      it "checks address_from is complete" do
        creds = { key: "123", password: "123", account_number: "1234", meter_number: "2987" }
        wrong_quote = {
          address_from: { street_lines: "Filemon 43", city: "GDL", state_code: "JAL", zip: "123" },
          addres_to: { zip: "123", country: "USA" },
          parcel: { length: 1, width: 1, height: 2, distance_unit: "cm", weight: 2, mass_unit: "kg" }
        }

        expect { Fedex::Rates::Payload.build(creds, wrong_quote) }
          .to raise_error(Fedex::PayloadError, "address_from.country is missing")
      end

      it "checks for errors in order, stops at the first one" do
        creds = { key: "123", password: "123", account_number: "1234", meter_number: "2987" }
        # address_from.country has an incorrect format,
        # address.to has no country, but that error won't be raised
        wrong_quote = {
          address_from: { street_lines: "Filemon 43", city: "GDL", state_code: "JAL", zip: "123", country: "MXaaa" },
          addres_to: { zip: "123" },
          parcel: { length: 1, width: 1, height: 2, distance_unit: "cm", weight: 2, mass_unit: "kg" }
        }

        expect { Fedex::Rates::Payload.build(creds, wrong_quote).build }
          .to raise_error(Fedex::PayloadError, "address_from.country is in invalid format")

        expect { Fedex::Rates::Payload.build(creds, wrong_quote).build }
          .not_to raise_error(Fedex::PayloadError, "address_to.country is missing")
      end
    end
  end

  def sample_xml_payload
    <<-XML
      <RateRequest xmlns=\"http://fedex.com/ws/rate/v13\">
        <WebAuthenticationDetail>
          <UserCredential>
            <Key>1</Key>
            <Password>2</Password>
          </UserCredential>
        </WebAuthenticationDetail>
        <ClientDetail>
          <AccountNumber>3</AccountNumber>
          <MeterNumber>4</MeterNumber>
          <Localization>
            <LanguageCode>es</LanguageCode>
            <LocaleCode>MX</LocaleCode>
          </Localization>
        </ClientDetail>
        <Version>
          <ServiceId>crs</ServiceId>
          <Major>13</Major>
          <Intermediate>0</Intermediate>
          <Minor>0</Minor>
        </Version>
        <ReturnTransitAndCommit>true</ReturnTransitAndCommit>
        <RequestedShipment>
          <DropoffType>REGULAR_PICKUP</DropoffType>
          <PackagingType>YOUR_PACKAGING</PackagingType>
          <Shipper>
            <Address>
              <StreetLines></StreetLines>
              <City></City>
              <StateOrProvinceCode>XX</StateOrProvinceCode>
              <PostalCode>64000</PostalCode>
              <CountryCode>MX</CountryCode>
            </Address>
          </Shipper>
          <Recipient>
            <Address>
              <StreetLines></StreetLines>
              <City></City>
              <StateOrProvinceCode>XX</StateOrProvinceCode>
              <PostalCode>06500</PostalCode>
              <CountryCode>MX</CountryCode>
              <Residential>false</Residential>
            </Address>
          </Recipient>
          <ShippingChargesPayment>
            <PaymentType>SENDER</PaymentType>
          </ShippingChargesPayment>
          <RateRequestTypes>ACCOUNT</RateRequestTypes>
          <PackageCount>1</PackageCount>
          <RequestedPackageLineItems>
            <GroupPackageCount>1</GroupPackageCount>
            <Weight>
              <Units>KG</Units>
              <Value>1</Value>
            </Weight>
            <Dimensions>
              <Length>1.0</Length>
              <Width>1.0</Width>
              <Height>1.0</Height>
              <Units>CM</Units>
            </Dimensions>
          </RequestedPackageLineItems>
        </RequestedShipment>
      </RateRequest>
    XML
  end
end
